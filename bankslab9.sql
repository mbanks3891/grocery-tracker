
/*NOTE these queries are not run from this file, they are pasted into and run from within SQL workbench.
this file is used for ogranziation/layout of queries I'm using to build tables and populate those 
tables with data


LINE 271 demonstrates a join that pulls data from all tables into the intended representation of an item

*/

--start BRAND TABLE-------------------------------------------------------------------------------------

DELETE FROM brand;

ALTER TABLE brand AUTO_INCREMENT=1;

INSERT INTO brand (brandname, parentcompany)
VALUES('Great Value', 'Walmart')
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Horizon","Dean Foods")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Land O'Lakes","Land O'Lakes")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Ragu","Unilever")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Raisin Bran","Raisin Bran")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Planters","Kraft Heinz")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Campbell's","Campbell's")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Superior Nut Company","Superior Nut Company")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Hidden Valley","Clorox")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Tostitos","Frito Lay")
;

INSERT INTO brand (brandname, parentcompany)
VALUES("Gatorade","PepsiCo")
;
--end BRAND TABLE-------------------------------------------------------------------------------------

--start ITEM TABLE------------------------------------------------------------------------------------

DELETE FROM item;

ALTER TABLE item AUTO_INCREMENT=1;



INSERT INTO item (category_idCategory, brand_idBrand, myItemName, storeItemName, 
store_idStore, storeSKU, purchaseDate, currentPrice, previousPrice, 
package_idPkg, pricePerServing, pricePerComponent, pricePerPkg)
VALUES("5", "7", "raisin bran", "RAISIN BRAN", 
"1", "003800059661", "7-23-2019", "3.32", "3.64", 
"1", ".29", "3.32", "3.32")
;


INSERT INTO item (category_idCategory, brand_idBrand, myItemName, storeItemName, store_idStore, 
package_idPkg, storeSKU, purchaseDate, currentPrice, previousPrice, package_idPkg, 
pricePerServing, pricePerComponenet, pricePerPkg)
VALUES("", "", "", "", "", "", "", "", "", "", "", "", "")
;


INSERT INTO item (category_idCategory, brand_idBrand, myItemName, storeItemName, store_idStore, 
package_idPkg, storeSKU, purchaseDate, currentPrice, previousPrice, package_idPkg, 
pricePerServing, pricePerComponenet, pricePerPkg)
VALUES("", "", "", "", "", "", "", "", "", "", "", "", "")
;


INSERT INTO item (category_idCategory, brand_idBrand, myItemName, storeItemName, store_idStore, 
package_idPkg, storeSKU, purchaseDate, currentPrice, previousPrice, package_idPkg, 
pricePerServing, pricePerComponenet, pricePerPkg)
VALUES("", "", "", "", "", "", "", "", "", "", "", "", "")
;

INSERT INTO item (myItemName, storeItemName, storeSKU, purchaseDate, currentPrice, previousPrice,
stores_idStore, categories_idCategory, brand_idBrand, store_idStore)
VALUES("", "", "", "", "", "", "", "", "", "")
;

INSERT INTO item (myItemName, storeItemName, storeSKU, purchaseDate, currentPrice, previousPrice,
stores_idStore, categories_idCategory, brand_idBrand, store_idStore)
VALUES("", "", "", "", "", "", "", "", "", "")
;

INSERT INTO item (myItemName, storeItemName, storeSKU, purchaseDate, currentPrice, previousPrice,
stores_idStore, categories_idCategory, brand_idBrand, store_idStore)
VALUES("", "", "", "", "", "", "", "", "", "")
;

INSERT INTO item (myItemName, storeItemName, storeSKU, purchaseDate, currentPrice, previousPrice,
stores_idStore, categories_idCategory, brand_idBrand, store_idStore)
VALUES("", "", "", "", "", "", "", "", "", "")
;

INSERT INTO item (myItemName, storeItemName, storeSKU, purchaseDate, currentPrice, previousPrice,
stores_idStore, categories_idCategory, brand_idBrand, store_idStore)
VALUES("", "", "", "", "", "", "", "", "", "")
;


UPDATE item
SET package_idpkg = "4" 
WHERE idItem = 1
;


UPDATE item
SET pricePerServing = "0.11", pricePerComponent = "5.14", pricePerPkg = "5.14" 
WHERE idItem = 1
;



--end ITEM TABLE-------------------------------------------------------------------------------------


--start STORE TABLE-------------------------------------------------------------------------------------

INSERT INTO store (storeName, storeCode, storeAddress)
VALUES("Walmart", "02918", "730 W Exchange Pkwy, Allen, TX 75013")
;

UPDATE store
SET storeAddress = "730 W Exchange Pkwy, Allen, TX 75013"
WHERE idstore = 1
;

INSERT INTO store (storeName, storeCode, storeAddress)
VALUES("", "", "")
;



--end STORE TABLE-------------------------------------------------------------------------------------


--start CATEGORY TABLE-------------------------------------------------------------------------------------
INSERT INTO category (categoryName)
VALUES("beverage")
;

INSERT INTO category (categoryName)
VALUES("condiment")
;

INSERT INTO category (categoryName)
VALUES("dairy")
;

INSERT INTO category (categoryName)
VALUES("fruit")
;

INSERT INTO category (categoryName)
VALUES("grain")
;

INSERT INTO category (categoryName)
VALUES("meat")
;

INSERT INTO category (categoryName)
VALUES("legume")
;

INSERT INTO category (categoryName)
VALUES("snack")
;

INSERT INTO category (categoryName)
VALUES("veg")
;
-- beverage, condiment, dairy, fruit, grain, meat, legume, snack, veg


--end CATEGORY TABLE-------------------------------------------------------------------------------------


--start PACKAGE TABLE-------------------------------------------------------------------------------------

INSERT INTO package (pkgType, pkgQtyOfComponents, componentQty, componentUnit, 
componentFormat, pkgQty, pkgUnit, servingQty, servingUnit)
VALUES(
"single", "1", "24", "oz", "jar", "24", "oz", "6", "oz"
)
;





UPDATE package
SET servingsPerComponent="4.0", servingsPerPkg=("4.0")
WHERE idpkg=1
;

DELETE from package
WHERE idpkg=2
;

ALTER TABLE package AUTO_INCREMENT=2;

INSERT INTO package (pkgType, pkgQtyOfComponents, componentQty, componentUnit, 
componentFormat, pkgQty, pkgUnit, servingQty, servingUnit, servingsPerComponent, servingsPerPkg)
VALUES(
"single", "1", "5.5", "oz", "can", "5.5", "oz", "30", "g", "5.13", "5.13"
)
;

INSERT INTO package (pkgType, pkgQtyOfComponents, componentQty, componentUnit, 
componentFormat, pkgQty, pkgUnit, servingQty, servingUnit, servingsPerComponent, servingsPerPkg)
VALUES(
"single", "1", "24", "oz", "jar", "24", "oz", "6", "oz", "4.0", "4.0"
)
;

INSERT INTO package (pkgType, pkgQtyOfComponents, componentQty, componentUnit, 
componentFormat, pkgQty, pkgUnit, servingQty, servingUnit, servingsPerComponent, servingsPerPkg)
VALUES(
"multi", "6", "11.5", "oz", "can", "69.0", "oz", "1", "oz", "11.5", "69.0"
)
;

INSERT INTO package (pkgType, pkgQtyOfComponents, componentQty, componentUnit, 
componentFormat, pkgQty, pkgUnit, servingQty, servingUnit, servingsPerComponent, servingsPerPkg)
VALUES(
"single", "1", "24", "oz", "tub", "24", "oz", "1", "tbsp", "48", "48"
)
;

--end PACKAGE TABLE-------------------------------------------------------------------------------------




--start PRICE TABLE-------------------------------------------------------------------------------------

INSERT INTO price (pricePerServing, pricePerComponent, pricePerPkg)
VALUES("","","")
;

--end PRICE TABLE---------------------------------------------------------------------------------------





SELECT s.storeName, c.categoryName, i.storeItemName, b.brandName, i.myItemName, p.pkgQtyOfComponents, p.componentQty, p.componentUnit, 
p.componentFormat, i.currentPrice, i.pricePerComponent, i.pricePerServing
FROM item i
LEFT JOIN package p
ON p.idPkg = i.package_idPkg
LEFT JOIN category c
ON c.idCategory = i.category_idCategory
LEFT JOIN brand b
ON b.idBrand = i.brand_idBrand
LEFT JOIN store s
ON s.idStore = i.store_idStore
;







-- USING MY GUITAR SHOP
select * from addresses a
left join customers c
on a.customer_id = c.customer_id;

select * from customers;


select * from addresses;

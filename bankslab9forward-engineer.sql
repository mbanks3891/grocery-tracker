-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema grocery
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grocery
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grocery` DEFAULT CHARACTER SET utf8 ;
USE `grocery` ;

-- -----------------------------------------------------
-- Table `grocery`.`Store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Store` (
  `idStore` INT NOT NULL AUTO_INCREMENT,
  `storeName` VARCHAR(45) NULL,
  `storeCode` INT NULL,
  `storeAddress` VARCHAR(45) NULL,
  PRIMARY KEY (`idStore`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Category` (
  `idCategory` INT NOT NULL AUTO_INCREMENT,
  `categoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Price` (
  `idPrice` INT NOT NULL AUTO_INCREMENT,
  `pricePerServing` FLOAT NOT NULL,
  `pricePerComponent` FLOAT NOT NULL,
  `pricePerPkg` FLOAT NOT NULL,
  PRIMARY KEY (`idPrice`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Package` (
  `idPkg` INT NOT NULL AUTO_INCREMENT,
  `pkgType` VARCHAR(45) NULL,
  `pkgQtyOfComponents` FLOAT NULL,
  `componentQty` FLOAT NULL,
  `componentUnit` FLOAT NULL,
  `componentFormat` FLOAT NULL,
  `pkgQty` FLOAT NULL,
  `pkgUnit` FLOAT NULL,
  `servingQty` FLOAT NULL,
  `servingUnit` FLOAT NULL,
  `servingsPerComponent` FLOAT NULL,
  `servingsPerPkg` FLOAT NULL,
  PRIMARY KEY (`idPkg`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Brand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Brand` (
  `idBrand` INT NOT NULL AUTO_INCREMENT,
  `brandName` VARCHAR(45) NOT NULL,
  `parentCompany` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idBrand`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Item` (
  `idItem` INT NOT NULL AUTO_INCREMENT,
  `myItemName` VARCHAR(45) NULL,
  `storeItemName` VARCHAR(45) NULL,
  `storeSKU` INT NULL,
  `purchaseDate` VARCHAR(45) NULL,
  `currentPrice` FLOAT NULL,
  `previousPrice` FLOAT NULL,
  `Stores_idStore` INT NOT NULL,
  `Categories_idCategory` INT NOT NULL,
  `Price_idPrice` INT NOT NULL,
  `Package_idPkg` INT NOT NULL,
  `Brand_idBrand` INT NOT NULL,
  `Store_idStore` INT NOT NULL,
  PRIMARY KEY (`idItem`, `Stores_idStore`, `Categories_idCategory`, `Price_idPrice`, `Package_idPkg`, `Brand_idBrand`, `Store_idStore`),
  INDEX `fk_Items_Categories_idx` (`Categories_idCategory` ASC) VISIBLE,
  INDEX `fk_Item_Price1_idx` (`Price_idPrice` ASC) VISIBLE,
  INDEX `fk_Item_Package1_idx` (`Package_idPkg` ASC) VISIBLE,
  INDEX `fk_Item_Brand1_idx` (`Brand_idBrand` ASC) VISIBLE,
  INDEX `fk_Item_Store1_idx` (`Store_idStore` ASC) VISIBLE,
  CONSTRAINT `fk_Items_Categories`
    FOREIGN KEY (`Categories_idCategory`)
    REFERENCES `grocery`.`Category` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Price1`
    FOREIGN KEY (`Price_idPrice`)
    REFERENCES `grocery`.`Price` (`idPrice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Package1`
    FOREIGN KEY (`Package_idPkg`)
    REFERENCES `grocery`.`Package` (`idPkg`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Brand1`
    FOREIGN KEY (`Brand_idBrand`)
    REFERENCES `grocery`.`Brand` (`idBrand`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Store1`
    FOREIGN KEY (`Store_idStore`)
    REFERENCES `grocery`.`Store` (`idStore`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;











-- **** FORGOT AUTO INCREMENTS- updated

-- new Schema name is 'Grocery'


-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema grocery
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema grocery
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `grocery` DEFAULT CHARACTER SET utf8 ;
USE `grocery` ;

-- -----------------------------------------------------
-- Table `grocery`.`Store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Store` (
  `idStore` INT NOT NULL AUTO_INCREMENT,
  `storeName` VARCHAR(45) NULL,
  `storeCode` INT NULL,
  `storeAddress` VARCHAR(45) NULL,
  PRIMARY KEY (`idStore`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Category` (
  `idCategory` INT NOT NULL,
  `categoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Price` (
  `idPrice` INT NOT NULL,
  `pricePerServing` FLOAT NOT NULL,
  `pricePerComponent` FLOAT NOT NULL,
  `pricePerPkg` FLOAT NOT NULL,
  PRIMARY KEY (`idPrice`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Package` (
  `idPkg` INT NOT NULL,
  `pkgType` VARCHAR(45) NULL,
  `pkgQtyOfComponents` FLOAT NULL,
  `componentQty` FLOAT NULL,
  `componentUnit` FLOAT NULL,
  `componentFormat` FLOAT NULL,
  `pkgQty` FLOAT NULL,
  `pkgUnit` FLOAT NULL,
  `servingQty` FLOAT NULL,
  `servingUnit` FLOAT NULL,
  `servingsPerComponent` FLOAT NULL,
  `servingsPerPkg` FLOAT NULL,
  PRIMARY KEY (`idPkg`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Brand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Brand` (
  `idBrand` INT NOT NULL,
  `brandName` VARCHAR(45) NOT NULL,
  `parentCompany` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idBrand`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `grocery`.`Item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `grocery`.`Item` (
  `idItem` INT NOT NULL,
  `myItemName` VARCHAR(45) NULL,
  `storeItemName` VARCHAR(45) NULL,
  `storeSKU` INT NULL,
  `purchaseDate` VARCHAR(45) NULL,
  `currentPrice` FLOAT NULL,
  `previousPrice` FLOAT NULL,
  `Stores_idStore` INT NOT NULL,
  `Categories_idCategory` INT NOT NULL,
  `Price_idPrice` INT NOT NULL,
  `Package_idPkg` INT NOT NULL,
  `Brand_idBrand` INT NOT NULL,
  `Store_idStore` INT NOT NULL,
  PRIMARY KEY (`idItem`, `Stores_idStore`, `Categories_idCategory`, `Price_idPrice`, `Package_idPkg`, `Brand_idBrand`, `Store_idStore`),
  INDEX `fk_Items_Categories_idx` (`Categories_idCategory` ASC) VISIBLE,
  INDEX `fk_Item_Price1_idx` (`Price_idPrice` ASC) VISIBLE,
  INDEX `fk_Item_Package1_idx` (`Package_idPkg` ASC) VISIBLE,
  INDEX `fk_Item_Brand1_idx` (`Brand_idBrand` ASC) VISIBLE,
  INDEX `fk_Item_Store1_idx` (`Store_idStore` ASC) VISIBLE,
  CONSTRAINT `fk_Items_Categories`
    FOREIGN KEY (`Categories_idCategory`)
    REFERENCES `grocery`.`Category` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Price1`
    FOREIGN KEY (`Price_idPrice`)
    REFERENCES `grocery`.`Price` (`idPrice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Package1`
    FOREIGN KEY (`Package_idPkg`)
    REFERENCES `grocery`.`Package` (`idPkg`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Brand1`
    FOREIGN KEY (`Brand_idBrand`)
    REFERENCES `grocery`.`Brand` (`idBrand`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Store1`
    FOREIGN KEY (`Store_idStore`)
    REFERENCES `grocery`.`Store` (`idStore`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;














-- OLD NAME 'mydb'

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Store`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Store` (
  `idStore` INT NOT NULL AUTO_INCREMENT,
  `storeName` VARCHAR(45) NULL,
  `storeCode` INT NULL,
  `storeAddress` VARCHAR(45) NULL,
  PRIMARY KEY (`idStore`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Category` (
  `idCategory` INT NOT NULL,
  `categoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategory`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Price`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Price` (
  `idPrice` INT NOT NULL,
  `pricePerServing` FLOAT NOT NULL,
  `pricePerComponent` FLOAT NOT NULL,
  `pricePerPkg` FLOAT NOT NULL,
  PRIMARY KEY (`idPrice`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Package`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Package` (
  `idPkg` INT NOT NULL,
  `pkgType` VARCHAR(45) NULL,
  `pkgQtyOfComponents` FLOAT NULL,
  `componentQty` FLOAT NULL,
  `componentUnit` FLOAT NULL,
  `componentFormat` FLOAT NULL,
  `pkgQty` FLOAT NULL,
  `pkgUnit` FLOAT NULL,
  `servingQty` FLOAT NULL,
  `servingUnit` FLOAT NULL,
  `servingsPerComponent` FLOAT NULL,
  `servingsPerPkg` FLOAT NULL,
  PRIMARY KEY (`idPkg`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Brand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Brand` (
  `idBrand` INT NOT NULL,
  `brandName` VARCHAR(45) NOT NULL,
  `parentCompany` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idBrand`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Item` (
  `idItem` INT NOT NULL,
  `myItemName` VARCHAR(45) NULL,
  `storeItemName` VARCHAR(45) NULL,
  `storeSKU` INT NULL,
  `purchaseDate` VARCHAR(45) NULL,
  `currentPrice` FLOAT NULL,
  `previousPrice` FLOAT NULL,
  `Stores_idStore` INT NOT NULL,
  `Categories_idCategory` INT NOT NULL,
  `Price_idPrice` INT NOT NULL,
  `Package_idPkg` INT NOT NULL,
  `Brand_idBrand` INT NOT NULL,
  `Store_idStore` INT NOT NULL,
  PRIMARY KEY (`idItem`, `Stores_idStore`, `Categories_idCategory`, `Price_idPrice`, `Package_idPkg`, `Brand_idBrand`, `Store_idStore`),
  INDEX `fk_Items_Categories_idx` (`Categories_idCategory` ASC) VISIBLE,
  INDEX `fk_Item_Price1_idx` (`Price_idPrice` ASC) VISIBLE,
  INDEX `fk_Item_Package1_idx` (`Package_idPkg` ASC) VISIBLE,
  INDEX `fk_Item_Brand1_idx` (`Brand_idBrand` ASC) VISIBLE,
  INDEX `fk_Item_Store1_idx` (`Store_idStore` ASC) VISIBLE,
  CONSTRAINT `fk_Items_Categories`
    FOREIGN KEY (`Categories_idCategory`)
    REFERENCES `mydb`.`Category` (`idCategory`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Price1`
    FOREIGN KEY (`Price_idPrice`)
    REFERENCES `mydb`.`Price` (`idPrice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Package1`
    FOREIGN KEY (`Package_idPkg`)
    REFERENCES `mydb`.`Package` (`idPkg`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Brand1`
    FOREIGN KEY (`Brand_idBrand`)
    REFERENCES `mydb`.`Brand` (`idBrand`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Item_Store1`
    FOREIGN KEY (`Store_idStore`)
    REFERENCES `mydb`.`Store` (`idStore`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

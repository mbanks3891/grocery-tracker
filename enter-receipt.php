<?php
session_start();
ob_start();
require_once 'functions.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>$pageTitleArg1</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<link rel="stylesheet" href="styles.css">



</head>
<body>


<div>
<header>
<h1 align="center">Grocery Tracker</h1>
<h3 align="center">UNDER CONSTRUCTION, 7-30-19</h2>
</header>
</div>


<?php
$valid=true;
$nostoreflag="";



if(isset($_POST['submititem']))
{

	$valid=true;
	$missingfields=0;
	

	//validation and gather data from form fields into php vars
	//----------------------------------------------------
	if (isset($_POST['category'])){
	$category=$_POST['category'];
	}
	else
	$category="";
	if(empty($category)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	$storename=$_POST['storename'];
	if(empty($storename)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	$sku=$_POST['sku'];
	if(empty($sku)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	$yourname=$_POST['yourname'];
	if(empty($yourname)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	$brand=$_POST['brand'];
	if(empty($brand)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	$price=$_POST['price'];
	if(empty($price)){
	$missingfields++;
	$valid=false;
	}
	//----------------------------------------------------
	if (isset($_POST['package'])){
	$package=$_POST['package'];
	}
	else
	$package="";
	if(empty($package)){
	$missingfields++;
	$valid=false;
	}

	
		if ($valid)
		{
			// for each valid item increment itemcount by 1 and store it in variable
			$_SESSION['itemcount']++;
			$itemcount= $_SESSION['itemcount'];
			$indexcount=$itemcount-1;
			// after registering this info for each valid item... reset item fields to blank

			//show this page again, with blank fields...   until 'DONE ADDING ITEMS' button is clicked
			// and keep a total of items added
			$store=$_SESSION['yourstore'];
			echo "your store is " . $store . "<br>";
			

				//if no array yet

				

				echo "data added<br>";
				print_r($_SESSION['array']);


			//with each submission, push new data to a new array position
			//start by replacing contents of array index 0
 			$_SESSION['array'][$indexcount]['category']=$category;
			$_SESSION['array'][$indexcount]['storename']=$storename;
			$_SESSION['array'][$indexcount]['sku']=$sku;
			$_SESSION['array'][$indexcount]['yourname']=$yourname;
			$_SESSION['array'][$indexcount]['brand']=$brand;
			$_SESSION['array'][$indexcount]['price']=$price;
			$_SESSION['array'][$indexcount]['package']=$package;

				
			//reset form fields to blank after each valid submission
			$category="";
			$storename="";
			$sku="";
			$yourname="";
			$brand="";
			$price="";
			$package="";
			
		}//end IF VALID
	 

		else //if not valid
		{
		$itemcount= $_SESSION['itemcount'];//need to display item count on form with error screen
		echo "<div id='msgtxt'>missing ".$missingfields." fields</div>";
		}


}//end if isset submit
else//if not sumbitted AKA first visit 
{
$_SESSION['itemcount']=0;//initialize session var 'itemcount' as 0

	//check for session variable 'your store' carried over from store selection page
	if (isset($_SESSION['yourstore']))
	{
		$store=$_SESSION['yourstore'];
		echo "your store is " . $store . "<br>";
		$itemcount=0;
	}
	else// no store session var set, AKA skipped store select page
	{
		$nostoreflag='style="display:none"';//hide the form
		$no_store_message="<a href=select-store.php>select a store</a>";//direct back to store select page
		echo "<div id='msgtxt'>no store selected! -----> ". $no_store_message ." to begin</div>";
	}





//initialize php variables as blank form fields for first visit
$category="";
$storename="";
$sku="";
$yourname="";
$brand="";
$price="";
$package="";
$indexcount=0;


//create the array with blank fields values
$itemArray=//index 0
[
['category'=> $category,'storename' => $storename,'sku' => $sku, 
'yourname'=>  $yourname,'brand'=> $brand, 'price'=> $price, 'package'=> $package]
];

$_SESSION['array']= $itemArray;





echo "new array created<br>";
print_r($itemArray);


}//end else/no submission

?>


<div id="content" <?php echo $nostoreflag;?>> 

	<main>


		<form id= "formtop" method="post" action="enter-receipt.php">
	


		<!--///////RADIO BUTTONS///-->
			<div id="innerformradio">

				Category:
				<br>
				<input type="radio" name="category" value="Beverage" id="category">
				<label for="category">beverage</label>

				<input type="radio" name="category" value="Condiment">
				<label for="category">condiment</label>

				<input type="radio" name="category" value="Dairy">
				<label for="category">dairy</label>

				<br>
				<input type="radio" name="category" value="Fruit">
				<label for="category">fruit</label>


				<input type="radio" name="category" value="Grain">
				<label for="category">grain</label>

				<input type="radio" name="category" value="Leg">
				<label for="category">legume</label>


				<input type="radio" name="category" value="Meat">
				<label for="category">meat</label>

				<input type="radio" name="category" value="Snack">
				<label for="category">snack</label>

			</div><!-- end inner form radio-->



			<div id="innerformcolumns">

				<div id="innerformleft">


					<!--///////TEXT///-->
					<p>
					<label for="">Store Item Name</label><br>
					<input type="text" name="storename" value="<?php echo $storename;?>" id="storename">


					<!--///////TEXT///-->
					<p>
					<label for="">SKU</label><br>
					<input type="text" name="sku" value="<?php echo $sku;?>" id="sku">

					<!--///////TEXT///-->
					<p>
					<label for="">Your Item Name</label><br>
					<input type="text" name="yourname" value="<?php echo $yourname;?>" id="yourname">


					<!--///////TEXT///-->
					<p>
					<label for="brand">Brand</label><br>
					<input type="text" name="brand" value="<?php echo $brand;?>" id="brand">

					<!--///////TEXT///-->					
					<p>
					<label for="package">Price</label><br>
					<input type="text" name="price" value="<?php echo $price;?>" id="price">



				</div><!-- end inner form left-->

				<div id="innerformright">
					
					<p>
					<label >Package</label>
					<select name="package" id="package">
					<option value="">Select a package</option>
					<option value="20 oz bottle">20 oz bottle</option>
					<option value="24 oz jar">24 oz jar</option>
					<option value="5.5 oz can">5.5 oz can</option>
					<option value="51 fl oz bottle">51 fl oz bottle</option>
					</select>

					<p id="pkg"> 
					<a href="#" >add a new package</a>
					</p>

					
					<br>

					<input id="submit" type="submit" name="submititem" value="SUMBIT ITEM">
					
					<p>
					<?php
					echo "items added: " . $itemcount ."<br>";
					echo"<br>";
					?>


				</div><!-- end inner form right-->


			</div><!-- end innerformcolumns-->

		</form>

	</main>


<section>

	<div id="rightcolumn">

		<div id="righttop">
			Items Added: <?php echo $itemcount;?>
	</div>





<div id="rightmid">




<?php
if($valid){
//for($y=$itemcount; $y>0; $y--)
//{
	for($x=$indexcount; $x>=0; $x--)
	{

	echo"
	<table>
	  <tr>
	    <th>Category:</th>
	    <td>".$_SESSION['array'][$x]['category']."</td></tr>";
	echo"
	  <tr>
	    <th>Store Name:</th>
	    <td>".$_SESSION['array'][$x]['storename']."</td></tr>";
	echo"
	  <tr>
	    <th>SKU:</th>
	    <td>".$_SESSION['array'][$x]['sku']."</td></tr>";
	echo"
	  <tr>
	    <th>Your Name:</th>
	    <td>".$_SESSION['array'][$x]['yourname']."</td></tr>";
	echo"
	  <tr>
	    <th>Brand:</th>
	    <td>".$_SESSION['array'][$x]['brand']."</td></tr>";
	echo"
	  <tr>
	    <th>Price:</th>
	    <td>".$_SESSION['array'][$x]['price']."</td></tr>";
	echo"
	  <tr>
	    <th>PKG:</th>
	    <td>".$_SESSION['array'][$x]['package']."</td></tr>
	</table>";

	echo"<br>----------------------------------------";
	}// end inner loop

//}// end outer loop

}//end if valid
?>
<!--

-->
</table>






			
</div>

			<div id="rightbottom">
				<input type="submit" name="done" value="DONE ADDING ITEMS">
			</div>

		</div>

	</section>

</div><!--end div id=conent-->





<footer>

<br>
<div id="home">
<a href="../bootstrap-index.php">HOME<a/>
</div>
<br>

<br>
&copy Copyright by Michael Banks, 2019
<br>
<br>

</footer>



</body>

</html>
<?php

function priceCalc($priceArg,$quantityArg){
$discountArray=array(0,0,.05,.1,.2,.25);//no 1.5 per instructions
if ($quantityArg > 5){$quantityArg=5;}
$discountPrice=$priceArg-($priceArg*$discountArray[$quantityArg]);
$total=$discountPrice*$quantityArg;
return $total;
}


function writeHead($pageTitleArg1, $pageTitleArg2){
$headText=<<<EOD
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>$pageTitleArg1</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<link rel="stylesheet" href="styles.css">
</head>
<body>


<div>
<header>
<h1 align="center">$pageTitleArg1</h1>
<h3 align="center">$pageTitleArg2</h2>
</header>
</div>


<br>
EOD;
echo $headText;
}


function writeFoot(){
$footText=<<<EOD
<footer>

<br>
<br>

<br>
&copy Copyright by Michael Banks, 2019
<br>
<br>

</footer>

</div>

</body>

</html>
EOD;

echo $footText;
}









?>